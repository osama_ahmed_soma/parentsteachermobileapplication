import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Storage} from '@ionic/storage';

// Config
import {Config} from '../../config/main.config';

// Providers
import {UserProvider} from '../user/user';

/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthProvider {

  tokenData: any = null;

  constructor(private http: HttpClient, private storage: Storage, private userService: UserProvider) {
    console.log('Hello AuthProvider Provider');
  }

  validate() {
    return new Promise((resolve) => {
      this.storage.get('authenticatedToken').then(data => {
        this.tokenData = data;
        if (!this.tokenData) {
          return resolve('WelcomePage');
        }
        this.refreshToken().then(() => {
          this.userService.getUser(this.tokenData.access_token).subscribe(user => {
            this.storeData(null, user);
            resolve('HomePage');
          }, err => {
            resolve('WelcomePage');
          });
        });
      });
    });
  }

  private refreshToken() {
    return new Promise(resolve => {
      const currentDateTimeInSeconds = parseInt(((Date.now() % 60000) / 1000).toFixed(0));
      const added = parseInt(((this.tokenData.added % 60000) / 1000).toFixed(0));
      if ((currentDateTimeInSeconds + 28800) >= (added + this.tokenData.expires_in)) {
        // refresh token
        this.updateToken().subscribe(res => {
          // this.setTokens(res);
          this.storeData(res);
          resolve(true);
        });
      } else {
        // all good
        resolve(false);
      }
    });
  }

  private updateToken() {
    const headers = new HttpHeaders({
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": `Bearer ${this.tokenData.access_token}`
    });
    const credentials = {
      grant_type: 'refresh_token',
      refresh_token: this.tokenData.refresh_token,
      client_id: Config.authClientCredentials.client_id,
      client_secret: Config.authClientCredentials.client_secret
    };
    return this.http.post(Config.domain + '/oauth/token', credentials, {
      headers
    });
  }

  storeData(authenticatedToken = null, userData = null) {
    if (authenticatedToken) {
      authenticatedToken.added = Date.now();
      this.storage.set('authenticatedToken', authenticatedToken);
    }
    if (userData) {
      this.storage.set('ptc_userData', userData);
    }
  }

  login(data: any) {
    const headers = new HttpHeaders({
      "Accept": "application/json",
      "Content-Type": "application/json"
    });
    const loginCredentials = {
      grant_type: 'password',
      client_id: Config.authClientCredentials.client_id,
      client_secret: Config.authClientCredentials.client_secret,
      username: data.phone_number,
      password: data.password
    };
    return this.http.post(Config.domain + '/oauth/token', loginCredentials, {
      headers
    });
  }

  logout() {
    this.storage.remove('authenticatedToken');
    this.storage.remove('ptc_userData');
    // return this.storage.remove('ptc_userData');
  }

}
