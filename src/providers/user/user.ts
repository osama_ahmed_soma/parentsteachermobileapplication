import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';

// Config
import {Config} from '../../config/main.config';

/*
  Generated class for the UserProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserProvider {

  endpoint: string = Config.api + '/user';

  constructor(public http: HttpClient) {
    console.log('Hello UserProvider Provider');
  }

  getUser(access_token) {
    const headers = new HttpHeaders({
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": "Bearer " + access_token
    });
    return this.http.get(this.endpoint, {
      headers
    });
  }

  getStudents(access_token) {
    const headers = new HttpHeaders({
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Authorization": "Bearer " + access_token
    });
    return this.http.get(`${this.endpoint}/students`, {
      headers
    });
  }

}
