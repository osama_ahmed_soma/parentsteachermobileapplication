import {Component, ViewChild} from '@angular/core';
import {IonicPage, NavController, NavParams, Content} from 'ionic-angular';
import {Storage} from '@ionic/storage';

// Providers
import {UserProvider} from '../../providers/user/user';
import {MessagePage} from "../message/message";

/**
 * Generated class for the DashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {

  @ViewChild(Content) content: Content;

  searchQuery: string = '';
  userData: any = null;
  students: Array<any> = [];
  studentsComplete: Array<any> = [];

  isLoading: boolean = true;
  access_token: string = '';

  constructor(private navCtrl: NavController,
              private navParams: NavParams,
              private storage: Storage,
              private userService: UserProvider,) {

  }

  ionViewDidLoad() {
    this.storage.get('authenticatedToken').then(data => {
      this.access_token = data.access_token;
      this.storage.get('ptc_userData').then(userData => {
        this.userData = userData.data;
      });
      this.userService.getStudents(this.access_token).subscribe((studentData: any) => {
        this.students = studentData.data;
        this.studentsComplete = this.students;
        this.isLoading = false;
        this.content.resize();
      });
    });
  }

  private declareStudents() {
    this.students = this.studentsComplete;
  }

  searchQueryInput(event) {
    this.declareStudents();
    if (this.searchQuery.trim() !== '') {
      this.students = this.students.filter(user => {
        if (user.name.toLowerCase().indexOf(this.searchQuery.toLowerCase()) > -1) {
          return user.name.toLowerCase().indexOf(this.searchQuery.toLowerCase()) > -1;
        } else if (user.meta.roll_number.toLowerCase().indexOf(this.searchQuery.toLowerCase()) > -1) {
          return user.meta.roll_number.toLowerCase().indexOf(this.searchQuery.toLowerCase()) > -1;
        } else {
          return user.parent.name.toLowerCase().indexOf(this.searchQuery.toLowerCase()) > -1;
        }
      });
    }
  }

  openMessage(student) {
    this.navCtrl.push('MessagePage', {
      student
    });
  }

  searchCancel(event) {
    console.log(event);
    this.declareStudents();
  }

}
