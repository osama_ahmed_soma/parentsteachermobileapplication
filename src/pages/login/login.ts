import {Component} from '@angular/core';
import {IonicPage, NavController, LoadingController, ToastController} from 'ionic-angular';

// Providers
import {AuthProvider} from '../../providers/auth/auth';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  userData: {
    phone_number: string,
    password: string
  } = {
    phone_number: '',
    password: ''
  };

  constructor(private navCtrl: NavController,
              private loadingCtrl: LoadingController,
              private toastCtrl: ToastController,
              private authService: AuthProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  private presentToast(message, cssClass = 'error') {
    let toast = this.toastCtrl.create({
      message,
      duration: 3000,
      position: 'top',
      cssClass
    });

    toast.present();
  }

  private validatePhoneNumbers(loading) {
    let error = false;
    if (!(this.userData.phone_number.match(/^\d{10}$/)) && !(this.userData.phone_number.match(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/)) && !(this.userData.phone_number.match(/^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/))) {
      this.presentToast('Phone Number is not valid number.');
      error = true;
      loading.dismiss();
    }
    return error;
  }

  private validate(loading) {
    let error = false;
    if (this.userData.phone_number == '') {
      this.presentToast('Phone Number is required.');
      loading.dismiss();
      error = true;
    } else {
      error = this.validatePhoneNumbers(loading);
    }
    if (this.userData.password == '') {
      this.presentToast('Password is required.');
      loading.dismiss();
      error = true;
    }
    return error;
  }

  login() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    if (this.validate(loading)) {
      return;
    }
    this.authService.login(this.userData).subscribe(res => {
      loading.dismiss();
      this.presentToast('Login Successfully', 'success');
      this.authService.storeData(res);
      this.navCtrl.setRoot('HomePage', {}, {
        animate: true,
        direction: 'forward'
      });
    }, err => {
      loading.dismiss();
      this.presentToast(err.error.message);
    });
  }

}
